import os
os.system("cls")

import datetime
hari_ini = datetime.date.today()

def menu_utama():
    print(f'{"MENU UTAMA":^40}')
    print('-'*40)
    print('1.INFORMASI SALDO    3.SETOR TUNAI')
    print('2.PENARIKAN TUNAI    4.EXIT')

def menu_lain():
    print(f'{"TARIK TUNAI":^40}')
    print('-'*40)
    print('1. 100.000      3. 500.000')
    print('2. 300.000      4. JUMLAH LAIN')

#DATA REKENING
nama = 'riskial'
PIN = 111
saldo = 2000000

while True:
    input_pin = int(input('MASUKKAN PIN ATM ANDA\n'))
    if input_pin != PIN:
        continue

    if input_pin == PIN:
        menu_utama()
        pilih_menu = int(input('PILIH MENU : '))
        if pilih_menu == 1:
            print(f'\n{"SALDO ANDA":^40}')
            print(f'    RP{saldo:,}')
            lagi = int(input('LANJUT TRANSAKSI LAIN ?\n1. YA \n2.TIDAK\n'))
            if lagi == 1:
                continue
            else:
                break
        elif pilih_menu ==2:
            menu_lain()
            tarik_tunai = int(input('PILIH NOMINAL PENARIKAN: '))
            if tarik_tunai ==1:
                tarik_tunai= 100000
            elif tarik_tunai==2:
                tarik_tunai =300000
            elif tarik_tunai ==3:
                tarik_tunai = 500000
            else :
                tarik_tunai = int(input('MASUKKAN NOMINAL YANG ANDA INGINKAN\RP'))
            saldo -= tarik_tunai
            print(f'{"STRUK PENARIKAN":^40}')
            print('-'*40)
            print(f'Nama              : {nama}')
            print(f'Tarik tunai       : {tarik_tunai:,}')
            print(f'Tanggal penarikan : {hari_ini}')
            print(f'Sisa saldo        : {saldo:,}')
            lagi = int(input('LANJUT TRAKSAKSI YANG LAIN ?\n1. Ya \n2. TIDAK\n'))
            if lagi == 1:
                continue
            else:
                break
        elif pilih_menu == 3:
            setor = int(input('MASUKKAN NOMINAL SETOR \nRP'))
            saldo += setor
            print(f'{"STRUK STOR TUNAI":^40}')
            print('-'*40)
            print(f'Nama                  :{nama}')
            print(f'Setor Tunai           :{setor}')
            print(f'Tanggal Penarikan     :{hari_ini}')
            print(f'Sisa Saldo            :{saldo}')
            lagi = int(input('\nLANJUT TRANSAKSI LAIN ?\n1. ya \n2. TIDAK\n'))
            if lagi ==1:
                continue
            else:
                break
        else:
            break
    break
print('TERIMAKASIH ATAS KUNJUNGAN ANDA')

